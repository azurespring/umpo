<?php

namespace AzureSpring\Umpo\Model;

class Property
{
    const PREDICATE_EQ = 'EQ';
    const PREDICATE_NE = 'NE';

    private $name;

    private $value;

    private $private;

    private $predicate;

    public function __construct(string $name, $value = null, bool $private = false, ?string $predicate = null)
    {
        $this->name = $name;
        $this->value = $value;
        $this->private = $private;
        $this->predicate = $predicate;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getValue()
    {
        return $this->value;
    }

    public function isPrivate(): bool
    {
        return $this->private;
    }

    public function getPredicate(): ?string
    {
        return $this->predicate;
    }
}
