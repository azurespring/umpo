<?php

namespace AzureSpring\Umpo\Model;

class CustomEvent
{
    private $name;

    private $properties;

    public function __construct(string $name, array $properties)
    {
        $this->name = $name;
        $this->properties = array_map(
            function ($p) {
                return !is_string($p) ? $p : new Property($p);
            },
            $properties
        );
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getProperties(): array
    {
        return $this->properties;
    }
}
