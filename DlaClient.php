<?php

namespace AzureSpring\Umpo;

use AzureSpring\Umpo\Model\CustomEvent;
use AzureSpring\Umpo\Model\Property;

class DlaClient
{
    private $pdo;

    public function __construct(string $host, string $port, string $username, string $password, string $database = 'integration_biz_ump_databank')
    {
        $this->pdo = new \PDO("mysql:host={$host};port={$port};dbname={$database};charset=utf8mb4", $username, $password);
    }

    public function findCustomEvent(CustomEvent $event, \DateTimeInterface $begin, \DateTimeInterface $end, int $limit = 20, int $offset = 0)
    {
        [$select, $where, $_grouping, $parameters] = $this->preprocess($event, $begin, $end);
        $sql = "SELECT {$select}, svr_timestamp timestamp".
            " FROM dwd_ump_log_mini_program_event_di".
            " WHERE event_name = :name AND :begin <= svr_timestamp AND svr_timestamp < :end AND {$where}".
            " ORDER BY svr_timestamp LIMIT {$limit} OFFSET {$offset}";

        $statement = $this->pdo->prepare($sql);
        $statement->execute($parameters);

        return array_map(
            function ($data) use ($event) {
                $row = ['_timestamp' => (new \DateTimeImmutable())->setTimestamp($data['timestamp'] / 1e3)];
                foreach ($event->getProperties() as $p) {
                    if (!$p->isPrivate()) {
                        $row[$p->getName()] = $data["_{$p->getName()}"];
                    }
                }

                return $row;
            },
            $statement->fetchAll()
        );
    }

    public function countCustomEvent(CustomEvent $event, \DateTimeInterface $begin, \DateTimeInterface $end, int $limit = 20, int $offset = 0)
    {
        [$select, $where, $grouping, $parameters] = $this->preprocess($event, $begin, $end);
        $sql = "SELECT {$select}, count(*) n".
            " FROM dwd_ump_log_mini_program_event_di".
            " WHERE event_name = :name AND :begin <= svr_timestamp AND svr_timestamp < :end AND {$where}".
            " GROUP BY {$grouping}".
            " ORDER BY n DESC, {$grouping}".
            " LIMIT {$limit} OFFSET {$offset}";

        $statement = $this->pdo->prepare($sql);
        $statement->execute($parameters);

        return array_map(
            function ($data) use ($event) {
                $row = ['_count' => $data['n']];
                foreach ($event->getProperties() as $p) {
                    if (!$p->isPrivate()) {
                        $row[$p->getName()] = $data["_{$p->getName()}"];
                    }
                }

                return $row;
            },
            $statement->fetchAll()
        );
    }

    private function preprocess(CustomEvent $event, \DateTimeInterface $begin, \DateTimeInterface $end)
    {
        $select = [];
        $grouping = [];
        $where = [];
        $parameters = [
            'name' => $event->getName(),
            'begin' => 1e3 * $begin->getTimestamp(),
            'end' => 1e3 * $end->getTimestamp(),
        ];
        foreach ($event->getProperties() as $p) {
            $u = "json_extract_scalar(custom_properties, '$.{$p->getName()}')";
            $v = "_{$p->getName()}";
            if (!$p->isPrivate()) {
                $select[] = "{$u} {$v}";
                $grouping[] = $v;
            }

            switch ($p->getPredicate()) {
                case Property::PREDICATE_EQ:
                    if (null === $p->getValue()) {
                        $where[] = "{$u} IS NULL";
                    } else {
                        $where[] = "{$u} = :{$v}";
                        $parameters[$v] = $p->getValue();
                    }
                    break;

                case Property::PREDICATE_NE:
                    if (null === $p->getValue()) {
                        $where[] = "{$u} IS NOT NULL";
                    } else {
                        $where[] = "{$u} != :{$v}";
                        $parameters[$v] = $p->getValue();
                    }
                    break;
            }
        }

        return [
            implode(', ', $select),
            implode(' AND ', $where) ?: 'TRUE',
            implode(', ', $grouping),
            $parameters,
        ];
    }
}
