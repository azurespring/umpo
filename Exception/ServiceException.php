<?php

namespace AzureSpring\Umpo\Exception;

class ServiceException extends \RuntimeException implements Exception
{
}
